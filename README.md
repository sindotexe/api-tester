<h1 align=center>🧪 API Tester</h1>
<p align=center>
<br><i>The non-complicated way to test your APIs out.</i>
<br><br>
<img src=https://img.shields.io/circleci/build/github/sinmineryt/api-tester?style=for-the-badge>
<img src=https://img.shields.io/github/license/sinmineryt/api-tester?style=for-the-badge>
<img src=https://img.shields.io/github/v/tag/sinmineryt/api-tester?style=for-the-badge>
</p>
<h1></h1>

<h3>👋 About</h3>

APITester is an easy to use and free forever desktop application to easily test out your APIs.
It's built in React and Electron and is completly open-source.

<h3>💻 Installing</h3>

<b>1. </b> Download the latest release from <a href=https://github.com/sinmineryt/api-tester/releases>here</a><br>
<b>2. </b> Double-click the installer<br>
<b>3. </b> API-Tester should now be installed and you can use it!<br>

<h3>🔨 Building</h3>

To build the application if you do not trust the precompiled files or want to change something you're gonna need these programs:
- `node.js` (Install from [here](https://nodejs.org/))
- `yarn` (Install with `npm install -g yarn`)
- `npm` (Should come with node.js)

1. Clone the project:
```
git clone https://github.com/sinmineryt/api-tester.git
```
2. Install the dependencies
```
yarn install
```
3. Build / Make the application
```
yarn run make
```
