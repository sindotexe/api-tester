import * as React from 'react';
import {  Switch, Route } from "react-router-dom";
import Home from './home';
import Settings from './settings';

class Main extends React.Component {
    render(): JSX.Element {
        return (
        <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/settings" component={Settings} />
        </Switch>
        );
    }
}

export default Main;