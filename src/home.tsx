import * as React from 'react';

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

import Store from './vendor/store.js';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCog } from '@fortawesome/free-solid-svg-icons';

const store = new Store({
    defaults: {
        secret: "Keyboard Cat! 😳"
    },
    configName: 'userdata'
});

class Home extends React.Component {
    render(): JSX.Element {

        store.set("username", "h")

        const username = store.get("username");

        return(
        <>
            <Container className="d-flex" fluid>
                <div className="Welcome">
                    <h1>👋 Welcome{username ? ', ' + username : ''}!</h1>   
                </div>
                <div className="SettingsBar d-flex ml-auto h3 text-muted">
                    <div className="SettingsButton">
                        <Button type="link" target="_blank" href="#/settings">
                            <FontAwesomeIcon icon={faCog} />
                        </Button>
                    </div>
                </div>
            </Container>
            <hr />
            <Container fluid>
                <Form>
                    <Form.Row>
                    <Form.Group as={Col} className="col-sm-2" controlId="formMethodInput">
                        <Form.Control className="text-nowrap" as="select" custom>
                            <option>GET</option>
                            <option>POST</option>
                            <option>HEAD</option>
                            <option>PUT</option>
                            <option>DELETE</option>
                        </Form.Control>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formAddrInput">
                    <Form.Control type="text" placeholder="https://api.example.com/endpoint" />
                    </Form.Group>
                    </Form.Row>

                </Form>
            </Container>
        </>
        );
    }
}

export default Home;