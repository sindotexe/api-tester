import * as React from 'react';

import Store from './vendor/store.js';

import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';

import { remote } from 'electron';

const store = new Store({
    defaults: {
        secret: "Keyboard Cat! 😳"
    },
    configName: 'userdata'
});

class Settings extends React.Component {
    render(): JSX.Element {
        const username = store.get("username");

        remote.getCurrentWindow().setBounds({
            width: 350,
            height: 400
        });
        remote.getCurrentWindow().center();
        remote.getCurrentWindow().menuBarVisible = false;
        remote.getCurrentWindow().setTitle("Settings");

        return(
        <>
            <Container className="d-block" fluid>
                <h3>Settings</h3>
                <p>Username is {username}</p>
            </Container>
        </>
        );
    }
}

export default Settings;
