import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Main from './main';
import { HashRouter } from "react-router-dom";


function render() {
  ReactDOM.render(
    <HashRouter>
        <Main />
    </HashRouter>, 
    document.querySelector("#app"));
}

render();